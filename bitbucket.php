<?php

$post_data = json_decode(file_get_contents('php://input'), true);
if (!empty($post_data)) {
    $path = dirname(__FILE__) . '/logs/';
    $post_file = $path . 'post.txt';
    fn_log_write($post_data, $post_file);
}

passthru("git checkout master");

passthru("git reset --hard");

passthru("git fetch");

passthru("git rebase origin/master");

echo 'Good';

function fn_log_write($data, $file)
{
    $file = fopen($file, 'a');

    if (!empty($file)) {
        fputs($file, 'TIME: ' . date('Y-m-d H:i:s', time()) . "\n");
        fputs($file, fn_array2code_string($data) . "\n\n");
        fclose($file);
    }
}

function fn_array2code_string($object, $indent = 0, $type = '')
{
    $scheme = '';

    if ($type == '') {
        if (is_array($object)) {
            $type = 'array';
        } elseif (is_numeric($object)) {
            $type = 'integer';
        }
    }

    if ($type == 'array') {
        $scheme .= "array(";
        if (is_array($object)) {
            if (!empty($object)) {
                $scheme .= " \n";
            }
            foreach ($object as $k => $v) {
                $scheme .= str_repeat("\t", $indent + 1) . "'$k' => " . fn_array2code_string($v, $indent + 1). ", \n";
            }
        }
        $scheme .= str_repeat("\t", $indent) . ")";
    } elseif ($type == 'int' || $type == 'integer') {
        if ($object == '') {
            $scheme .= 0;
        } else {
            $scheme .= $object;
        }
    } else {
        $scheme = "'$object'";
    }

    return $scheme;
}

exit;
